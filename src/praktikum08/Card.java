package praktikum08;

public class Card implements Comparable<Card> {

	private Suite suite;
	private Rank rank;
	
	public Card(Suite suite, Rank rank) {
		this.suite = suite;
		this.rank = rank;
	}
	
	@Override
	public String toString() {
		return String.format("%s of %s", rank, suite);
	}

	@Override
	public int compareTo(Card other) {
		return Integer.compare(rank.getValue(), other.rank.getValue());
	}
	
	public Rank getRank() {
		return rank;
	}
	
}
