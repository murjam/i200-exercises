package praktikum08;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CardGame {

	public static ArrayList<Card> createCardSet() {
		ArrayList<Card> set = new ArrayList<Card>();

		for (Suite suite : Suite.values()) {
			for (Rank rank : Rank.values()) {
				set.add(new Card(suite, rank));
			}
		}

		return set;
	}
	
	public static Rank findPair(ArrayList<Card> hand) {
		Collections.sort(hand);
		Card lastCard = null;
		for (Card card : hand) {
			if (lastCard != null) {
				if (0 == lastCard.compareTo(card)) {
					return card.getRank();
				}
			}			
			lastCard = card;
		}
		return null;
	}

	public static void main(String[] args) {

		ArrayList<Card> set = createCardSet();
		Collections.shuffle(set);
		
		ArrayList<Card> hand = new ArrayList<Card>();

		int index = 0;
		while (hand.size() < 5) {
			hand.add(set.remove(index++));
		}
		
		System.out.println(set.size());
		
		
		
		for (Card card : hand) {
			System.out.println(card);
		}
		Rank pair = findPair(hand);
		System.out.println(pair != null ? "On paar " + pair : "Paari pole");
	}

}
