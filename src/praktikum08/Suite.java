package praktikum08;

public enum Suite {
	
	/** &spades; */
	SPADES,
	/** &hearts; */
	HEARTS,
	/** &diams; */
	DIAMONDS,
	/** &clubs; */
	CLUBS;
	
	public String toString() {
		String icon = "";
		switch (this) {
		case CLUBS:
			break;
		case DIAMONDS:
			break;
		case HEARTS:
			icon = "♥";
			break;
		case SPADES:
			break;
		}
		return icon + " " + super.toString();
	}

}
