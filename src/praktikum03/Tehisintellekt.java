package praktikum03;

import lib.TextIO;

public class Tehisintellekt {

	public static void main(String[] args) {
		
		System.out.println("Sisesta kaks vanust");
		
		int vanus1 = TextIO.getlnInt();
		int vanus2 = TextIO.getlnInt();
		
		int vahe = Math.abs(vanus1 - vanus2);
		
		if (vahe < 5) {
			System.out.println("Väga okei");
		}
		else if (vahe < 11) {
			System.out.println("Suht okei");
		}
		else if (vahe < 16) {
			System.out.println("Veits kahtlane");
		}
		else {
			System.out.println("Not okay");
		}
		
	}
	
}
