package praktikum03;

import lib.TextIO;

public class PaarisVoiPaaritu {

	public static void main(String[] args) {
		
		System.out.println("Sisesta üks täisarv");
		
		int arv = TextIO.getlnInt();
		int jaak = arv % 2;
		
		
		if (jaak == 0) {
			System.out.println("Paaris");
		}
		else {
			System.out.println("Paaritu");
		}
		
	}
	
}
