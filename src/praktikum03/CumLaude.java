package praktikum03;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {
		System.out.println("Keskmine hinne:");
		double keskmine = TextIO.getlnInt();
		
		if (keskmine < 0) {
			System.out.println("Hinne ei saa olla negatiivne");
			return;
		}
		
		System.out.println("Lõputöö hinne:");
		int loputoo = TextIO.getlnInt();
		
		if (keskmine > 4.5 && 5 == loputoo) {
			System.out.println("Saad Cum Laude");
		}
		else {
			System.out.println("Ei saa, proovi uuesti");
		}
		
	}
	
}
