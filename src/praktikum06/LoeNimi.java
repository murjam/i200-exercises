package praktikum06;

public class LoeNimi {

	public static void main(String[] nimed) {
		
		
		if (nimed.length > 0) {
			System.out.format("\n\nTere %s!\n\n", nimed[0]);
			
			if (nimed.length > 1) {
				System.out.print("Vau, Sul on sõpru ka või!? (");
				for (int i = 1; i < nimed.length; i++) {
					if (i != 1) { // esimene kord ei pane koma
						System.out.print(", ");
					}
					System.out.print(nimed[i]);
				}
				System.out.println(")\n");
			}
			
		}
		else {
			System.out.println("Sisesta parameetrina nimi ka");
		}
	
	}
	
}
