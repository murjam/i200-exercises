package praktikum06;

public class Summa {
	
	public static int summeeri(int... arvud) {
		return summa(arvud);
	}
	
	public static int summa(int[] arvud) {
		int summa = 0;
		
		for (int arv : arvud) {
			summa = summa + arv;
		}
		
		return summa;
	}

	public static void main(String[] args) {
		
		System.out.println(summeeri(5, 7, 2, 7, 99, 3, 6));
		
	    // sedasi saab meetodi välja kutsuda
	    int summa = summa(new int[] {4, 3, 1, 7, -1}); // 14
	    System.out.println(summa);
	}
}
