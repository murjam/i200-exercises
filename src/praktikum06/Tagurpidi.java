package praktikum06;

import java.util.ArrayList;
import java.util.Collections;

import lib.TextIO;

public class Tagurpidi {

	public static void main(String[] args) {
		
		int mituArvu = 5;
		ArrayList<Integer> arvud = new ArrayList<Integer>();
		System.out.format("Sisesta %d arvu:\n", mituArvu);
		
		for (int i = 0; i < mituArvu; i++) {
			int arv = TextIO.getlnInt();
			arvud.add(arv);
		}
		
		Collections.reverse(arvud);

		for (int i = arvud.size() - 1; i >= 0; i--) {
			System.out.print(" " + arvud.get(i));
		}
		
	}
	
}
