package praktikum06;

import java.util.ArrayList;
import java.util.Arrays;

import lib.TextIO;
import praktikum05.Suvaline;

public class LiisuHeitmine {

	public static void main(String[] args) {
		
		String[] nimed = new String[3];

		System.out.println("Sisesta " + nimed.length + " nime:");
		
		for (int i = 0; i < nimed.length; i++) {
			System.out.format("Sisesta %d. nimi: ", i + 1);
			nimed[i] = TextIO.getlnString();
		}
		
		// debug
		System.out.println(Arrays.toString(nimed));
		
		int suvalineIndeks = (int) (Math.random() * nimed.length); 
				//Suvaline.suvaline(0, nimed.length - 1);
		
		String suvalineNimi = nimed[suvalineIndeks];
		
		System.out.println("Suvaline nimi: " + suvalineNimi);
	}
	
}
