package praktikum02;

import lib.TextIO;

public class Grupid {

	public static void main(String[] args) {
		
		System.out.println("Sisesta inimeste arv:");
		int inimesi = TextIO.getlnInt();
		
		System.out.println("Sisesta grupi suurus:");
		int grupiSuurus = TextIO.getlnInt();
		
		int taisGruppe = inimesi / grupiSuurus;
		int jaak = inimesi % grupiSuurus;
		
		
		System.out.println("Saab moodustada " + taisGruppe
				+ " gruppi ja üle jääb " + jaak + " inimest.");
		
		System.out.format("Saab moodustada %d gruppi ja üle jääb %d inimest.",
				taisGruppe, jaak);
		
		
	}
	
}
