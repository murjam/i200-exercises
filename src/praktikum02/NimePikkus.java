package praktikum02;

import lib.TextIO;

public class NimePikkus {

	public static void main(String[] args) {
		
		System.out.println("Sisesta nimi");
		
		String nimi = TextIO.getlnString();
		
		// regex - regular expression
		nimi = nimi.replaceAll("[aA]", "b");
		
		System.out.println(nimi);
		
		int nimePikkus = nimi.length();
		System.out.println(nimePikkus);


	}

}
