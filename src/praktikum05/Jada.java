package praktikum05;

public class Jada {

	public static String jada(char taht, int mitu) {
		String tahed = "";

		for (int i = 0; i < mitu; i++) {
			tahed = tahed + taht;
		}

		return tahed;
	}
	
	public static void kuusk(int korgus) {
		// joonistame kuuse
		for (int i = 1; i < korgus; i++) {
			System.out.format("%" + korgus + "s%s\n", jada('/', i), jada('\\', i));
		}
		// kuuse jalg
		int jalapaksus = Math.max(korgus / 3, 1);
		for (int i = 0; i < jalapaksus; i++) {
			System.out.format("%" + (korgus + jalapaksus) + "s\n", jada('|', jalapaksus * 2));
		}
	}

	public static void main(String[] args) {
		int mitu = 3;
		System.out.println(jada('b', mitu)); // "bbb"
		System.out.println(jada('-', 10)); // "----------"
		
		kuusk(10);
		kuusk(3);
		kuusk(2);
		kuusk(5);
		kuusk(20);
	}

}
