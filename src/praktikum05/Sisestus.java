package praktikum05;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Sisestus {
	
	public static int kasutajaSisestus(int min, int max) {
		String kysimus = String.format("Sisesta arv vahemikus %d..%d: ", min, max);
		return kasutajaSisestus(kysimus, min, max);
	}
	
	public static int kasutajaSisestus(String kysimus, int min, int max) {
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < 3; i++) {
			System.out.println(kysimus);
			
			try {
				int sisestus = scanner.nextInt();
				if (sisestus >= min && sisestus <= max) {
					scanner.close();
					return sisestus;
				}
				System.out.println("Arv ei ole sobilik!");
			}
			catch (InputMismatchException e) {
				scanner.nextLine();
				System.out.println("Sisesta ikka mingi arv!");
			}
		}
		
		System.out.println("Kasutaja on jobu, valime ise arvu");
		return min;
	}
	
	public static void main(String[] args) {
		int arv = kasutajaSisestus(0, 10);
		System.out.println("Sisestasid arvu: " + arv);
	}

}
