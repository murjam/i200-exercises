package praktikum05;

public class KullVoiKiri {

	public static void main(String[] args) {
		
		String kriipsud = Jada.jada('-', 40);
		
		System.out.println(kriipsud);
		int suvaline = Suvaline.suvaline(0, 1);
		int sisestus = Sisestus.kasutajaSisestus("Kull (0) või kiri (1)?", 0, 1);
		System.out.println(kriipsud);
		
		// Õige! Tuli kiri!
		if (suvaline == sisestus) {
			System.out.print("Õige! ");
		}
		else {
			System.out.print("Vale! ");
		}
		
		if (suvaline == 0) {
			System.out.println("Tuli kull!");
		} else {
			System.out.println("Tuli kiri!");
		}
		
		System.out.format("%s! Tuli %s!",
				sisestus == suvaline ? "Õige" : "Vale",
				suvaline == 0 ? "kull" : "kiri");
		
		
		
	}
	
}
