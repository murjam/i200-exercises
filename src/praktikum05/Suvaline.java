package praktikum05;

public class Suvaline {

	public static int suvaline(int min, int max) {
		
		int variatsioone = max - min + 1;
		int suvaline = (int)(Math.random() * variatsioone) + min;
		
		return suvaline;
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			System.out.print(" " + suvaline(4, 8));
		}
	}
		
}
