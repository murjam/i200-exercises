package praktikum07;

public class Human {

	public String name;
	public int age;
	

	public Human(String name) {
		this.name = name;
	}
	
	public Human(String name, int age) {
		this(name);
		this.age = age;
	}
	
	public void greet() {
		if (age > 3) {
			System.out.format("Hello, my name is %s and I am %d years old.\n", name, age);
		}
		else {
			System.out.println("Boo boo");
		}
	}
	
	// annotatsioonid
	@Override
	public String toString() {
		return name;
	}
}











