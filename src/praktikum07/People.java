package praktikum07;

import java.util.ArrayList;

import lib.TextIO;

public class People {

	public static void main(String[] args) {

		ArrayList<Human> people = new ArrayList<>();

		while (true) {
			System.out.println("Insert a name");
			String name = TextIO.getlnString().trim();

			if (name.isEmpty()) {
				break;
			}
			
			Human human = new Human(name);
			people.add(human);
		}
		// debug
		System.out.println(people);
		
		for (Human human : people) {
			System.out.println("Insert the age of " + human.name + ".");
			human.age = TextIO.getlnInt();
		}
		
		for (Human human : people) {
			human.greet();
		}

	}

}
