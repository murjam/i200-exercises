package praktikum04;

public class Jadad {

	public static void main(String[] args) {

		System.out.print("10 kuni 1: ");
		for (int i = 0; i < 10; i++) {
			System.out.format("%d ", 10 - i);
		}
		System.out.println();
		
		System.out.print("paarisarvud 0 kuni 10: ");
		for (int i = 0; i <= 10; i += 2) {
			System.out.print(i + " ");
		}
		System.out.println();
		
		System.out.print("kolmega jaguvad arvud vahemikus 30 kuni 0: ");
		for (int i = 0; i <= 30; i += 3) {
			System.out.print((30 - i) + " ");
		}
		System.out.println();
	}

}
