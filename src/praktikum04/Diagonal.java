package praktikum04;

import lib.TextIO;

public class Diagonal {
	public static void main(String[] args) {

		int size;
		while (true) {
			System.out.println("Sisesta positiivne paaritu arv");
			size = TextIO.getlnInt();
			if (size % 2 == 1 && size > 0) {
				break;
			}
		}
		
		String line = "";
		for (int i = 0; i < size * 2 + 3; i++) {
			line += '-';
		}
		
		System.out.println(line);
		for (int row = 0; row < size; row++) {
			System.out.print("| ");
			for (int col = 0; col < size; col++) {
				// short if, inline if
				System.out.print(col == row || col + row == size - 1 ? "x " : ". ");
			}
			System.out.println("|");
		}
		
		System.out.println(line);
		
		
		
	}
}
